// Primjer 2.0
var naslovJSX = <h1>Diplomski rad</h1>

ReactDOM.render(naslovJSX, document.getElementById("container"))

// Primjer 3.0
// JednostavnaKomponenta.js
function jednostavnaKomponenta(props) {
    return (
        <h1>Diplomski rad</h1>
    )
}

export default jednostavnaKomponenta

// Primjer 3.1
// ParentKomponenta.js
import JednostavnaKomponenta from './JednostavnaKomponenta.js'

function parentKomponenta(props) {
    return (
        <div>
            <JednostavnaKomponenta />
            <h2>Web tehnologije</h2>
        </div>
    )
}

// Primjer 3.2
// skripta.js
import React from 'react'
import { render } from 'react-dom'
import ParentKomponenta from './ParentKomponenta.js'
import { Button } from '@material-ui/core';

render(<ParentKomponenta />, document.getElementById("container"))

// Primjer 4.0
class StatefulKomponenta extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: 'nmuratovic'
        }
    }

    handleClick = (e) => {
        this.setState({
            username: 'new username'
        })
    }
    render() {
        return (
            <div>
                <h1>{this.state.username}</h1>
                <Button onClick={this.handleClick}>Update state</Button>
                <JednostavnaKomponenta username={this.state.username}/>
            </div>
        )
    }
}

// Primjer 5.0
class ParentKomponenta extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: 'nmuratovic'
        }
    }
    render() {
        return (
            <div>
                <JednostavnaKomponenta username={this.state.username}/>
                <h2>Web tehnologije</h2>
            </div>
        )
    }   
}

export default ParentKomponenta

// Primjer 5.1
function JednostavnaKomponenta(props) {
    return (
        <h1>{props.username}</h1>
    )
}

export default JednostavnaKomponenta

// Primjer 5.2
function JednostavnaKomponenta(props) {
    return (
        props.username == 'nmuratovic'
            && <p> Uspjesno ste prijavljeni.</p>
            || <p> Niste prijavljeni </p>
    )
}

// Primjer 5.3
import DisplayQuote from './DisplayQuote.js'

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      quoteId: 1,
      quoteText: 'So many books, so little time.',
      rating: 0
    }
  }

  azurirajRating() {
    this.setState({
      rating: this.state.rating++
    })
  }

  render() {
    return(
      <div>
        <p>Parent komponenta</p>
        <DisplayQuote quote={this.state} 
          azurirajRating={this.azurirajRating}/>
      </div>
    )
  }
}

function DisplayQuote(props) {
  return (
    <div>
      <p>ID citata: {props.quoteId}</p>
      <p>Tekst citata: {props.quoteText}</p>
      <p>Rating citata: {props.rating}</p>
      <Button onClick={props.azurirajRating}>Like</Button>
    </div>
  )
}

// Primjer 6.0
import { createStore } from 'redux'
import reducer from './reducer.js'

const store = createStore(reducer)

export default store

// Primjer 6.1
import { Provider } from 'react-redux'
import store from './store.js'

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('container')
)

// Primjer 8.0
const initialState = {
  username: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_USER":
      return { 
        ...state, 
        user: action.payload 
      }
    default:
        return state
  }
}

export default reducer

case "ADD_USER":
  state.username = action.payload
  return state

// primjer 9.1
const Header = (props) => {
  return (
    <div>
      <AppBar>
        <Toolbar>
          <Typography>Quotes</Typography>
          <SimpleMenu />
        </Toolbar>
      </AppBar>
    </div>
  )
}

// primjer 9.2
class Content extends Component { 
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div>
        <AddQuote />
        <Sort />
        <QuoteList cards={this.props.quotes} />
        <Router />
      </div>
    )
  }
}

// Primjer 9.3
const QuoteList = (props) => {
  return (
      <GridList>
        props.cards.map(item => (
          <GridListTile>
            <Quote data={item} />
          </GridListTile>
        ))
      </GridList>
  )
}

// Primjer 9.4
const Quote = (props) => {
  return (
    <Card>
      <CardContent>
        <Typography>
          {props.data.title}
        </Typography>
        <Typography>
          "{props.data.text}"
        </Typography>
        <Typography>
          {props.data.author}
        </Typography>
        <Typography>
          Liked by {props.data.rating}
        </Typography>
      </CardContent> 
    </Card>
  )
}

// Primjer 9.5
const Router = (props) => {
  return (
    <Switch>
      <Route path='/login' component={Login} />
      <Route path='/register' component={Registration} />
      <Route path='/logout' component={Logout} />
      <PrivateRoute path='/profile' component={Profile} />
    </Switch>
  )
}

// Primjer 10.0
import { connect } from 'react-redux'

const mapStateToProps = state => {
  return {  
    quotes: state.quotes 
  }
}

const Quote = (props) => {
  // ... kod kao u primjeru 9.4

}

export default connect(mapStateToProps, null)(Quote)

// Primjer 10.1
import { addQuote } from './actions/index.js'

const mapDispatchToProps = dispatch => {
  return {
      addQuote: quote => dispatch(addQuote(quote))
  }
}

class AddQuote extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      title: '',
      quoteText: ''
    }
  }

  // Kod koji azurira interno stanje komponente
  handleTitleChange = (e) => {
    this.setState({
      title: e.target.value
    })
  }

  handleQuoteTextChange = (e) => {
    this.setState({
      quoteText: e.target.value
    })
  }

  addQuote = () => {
    const quote = {
      title: this.state.title,
      text: this.state.quoteText,
      author: this.props.author,
      date: new Date().toISOString(),
      rating: 0
    }
    this.props.addQuote(quote)
  }

  render() {
    return (
      <div>
        <TextField
          label="Title"
          value={this.state.title}
          onChange={this.handleTitleChange}
        />
        <TextField
          label="Description"
          value={this.state.quoteText}
          onChange={this.handleQuoteTextChange}
        />
        <Button onClick={this.addQuote}>Add quote</Button>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(AddQuote)