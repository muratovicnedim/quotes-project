import axios from 'axios'
import { 
    REMEMBER_USER, 
    REMOVE_USER, 
    FETCH_QUOTES, 
    ADD_QUOTE,
    DELETE_QUOTE,
    UPDATE_QUOTE_LIKES, 
    SAVE_LIKED_QUOTE,
    EDIT_QUOTE
} from '../constants/action-types'

export const rememberUser = (username, password) => {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: 'http://localhost:5000/api/login',
            params: {
                username: username,
                password: password
            }
        })
        .then(response => {
            dispatch ({
                type: REMEMBER_USER,
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export const registerUser = (username, password) => {
    return (dispatch) => {
        return axios({
            method: 'post',
            url: 'http://localhost:5000/api/register',
            params: {
                username: username,
                password: password
            }
        })
        .then(response => {
            dispatch ({
                type: REMEMBER_USER,
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export const removeUser = () => {
    return {
        type: REMOVE_USER,
        payload: null
    }
}

export const addQuote = quote => {
    return (dispatch) => {
        return axios({
            method: 'post',
            url: 'http://localhost:5000/api/addquote',
            params: {
                title: quote.title,
                text: quote.text,
                author: quote.author,
                date: quote.date,
                rating: quote.rating
            }
        })
        .then(response => {
            dispatch({
                type: ADD_QUOTE,
                payload: response.data
              })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export const editQuote = quote => {
    return (dispatch) => {
        return axios({
            method: 'post',
            url: 'http://localhost:5000/api/editquote',
            params: {
                quoteId: quote.id,
                title: quote.title,
                text: quote.text
            }
        })
        .then(response => {
            const updatedQuote = {
                quoteId: quote.id,
                title: quote.title,
                text: quote.text
            }
            dispatch({
                type: EDIT_QUOTE,
                payload: updatedQuote
            })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export const deleteQuote = quoteId => {
    return (dispatch) => {
        return axios({
            method: 'post',
            url: 'http://localhost:5000/api/deletequote',
            params: {
                quoteId: quoteId
            }
        })
        .then(response => {
            dispatch({
                type: DELETE_QUOTE,
                payload: quoteId
            })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export const saveLikedQuote = (likedQuote, userId) => {
    return (dispatch) => {
        return axios({
            method: 'post',
            url: 'http://localhost:5000/api/savequote',
            params: {
                _id: likedQuote._id,
                title: likedQuote.title,
                text: likedQuote.text,
                author: likedQuote.author,
                date: likedQuote.date,
                rating: likedQuote.rating,
                userId: userId
            }
        })
        .then(response => {
            dispatch ({
                type: SAVE_LIKED_QUOTE,
                payload: response.data
            })
            dispatch ({
                type: UPDATE_QUOTE_LIKES,
                payload: {...likedQuote, rating: parseInt(likedQuote.rating, 10) + 1}
            })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export function fetchQuotes() {
    return (dispatch) => {
        return axios({
            method: 'get',
            url: 'http://localhost:5000/api/quotes'
        })
        .then(response => {
            dispatch ({
                type: FETCH_QUOTES,
                payload: response.data
            })
        })
        .catch(error => {
            console.log(error)
        })
    }
}

export const sortQuotes = sortCriteria => {
    return {
        type: "SORT_QUOTES",
        payload: sortCriteria
    }
}