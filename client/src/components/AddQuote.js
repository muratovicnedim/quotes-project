import React from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import Typography from '@material-ui/core/Typography'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { addQuote } from '../actions/index'

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: '2%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

const mapDispatchToProps = dispatch => {
  return {
      addQuote: quote => dispatch(addQuote(quote))
  }
}

const mapStateToProps = state => {
  return { 
    author:  state.user.username 
  }
}

class AddQuote extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      title: '',
      quoteText: ''
    }
  }

  handleTitleChange = (e) => {
    this.setState({
      title: e.target.value
    })
  }

  handleQuoteTextChange = (e) => {
    this.setState({
      quoteText: e.target.value
    })
  }

  addQuote = () => {
    const quote = {
      title: this.state.title,
      text: this.state.quoteText,
      author: this.props.author,
      date: new Date().toISOString(),
      rating: 0
    }
    this.props.addQuote(quote)
  }

  render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>Add new quote</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <div style={{ "width": "100%" }}>
            <TextField
              id="title"
              label="Title"
              margin="normal"
              value={this.state.title}
              onChange={this.handleTitleChange}
              helperText="Keep your title short"
            />
            <br />
            <TextField
              id="quote-text"
              label="Description"
              margin="normal"
              multiline="true"
              fullWidth="true"
              value={this.state.quoteText}
              onChange={this.handleQuoteTextChange}
              helperText="Do not submit dumb quotes"
            />
            <br />
            <Button variant="raised" onClick={this.addQuote} style={{ float: "right" }}> Add quote </Button>
          </div>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AddQuote))