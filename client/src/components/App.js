import React, { Component } from 'react'
import Header from './Header.js'
import Content from './Content.js'
import '../../stylesheets/app.css'

const App = (props) => (
  <div className="wrapper">
    <Header />
    <Content />
  </div>
)

export default App
