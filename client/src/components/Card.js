import React from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Tooltip from '@material-ui/core/Tooltip'
import { saveLikedQuote } from '../actions/index'
import { deleteQuote } from '../actions/index'
import ThumbUp from '@material-ui/icons/ThumbUp'
import Delete from '@material-ui/icons/Delete'
import Edit from '@material-ui/icons/Edit'
import '../../stylesheets/card.css'
import EditQuote from './EditQuote'

const mapDispatchToProps = dispatch => {
  return {
    saveLikedQuote: (quote, userId) => dispatch(saveLikedQuote(quote, userId)),
    deleteQuote: quoteId => dispatch(deleteQuote(quoteId))
  }
}

const mapStateToProps = state => {
  return { 
    loggedIn: state.user && true, 
    quotes: state.quotes, 
    user: state.user 
  }
}

const MyCard = (props) => {

  const like = () => {
    const quote = props.quotes.filter((element) => element._id == props.data._id)[0]
    props.saveLikedQuote(quote, props.user._id).then(data => {
      console.log('Quote successfuly saved.')
    })
  }

  const deleteQuote = () => {
    const quote = props.quotes.filter((element) => element._id == props.data._id)[0]
    console.log(quote._id)
    props.deleteQuote(quote._id).then(data => {
      console.log('Quote successfuly removed.')
    })
  }

  return (
    <div>
      <Card className="card" style={{boxShadow: "none", border: "1px solid #d4d4d4" }}>
        <CardContent>
          <Typography variant="title">
            {props.data.title}
          </Typography>
          <br/>
          <Typography color="primary" style={{ fontStyle: 'italic' }}>
            "{props.data.text}"
          </Typography>
          <Typography className="pos" color="default">
            <span style={{fontWeight: 450}}>{props.data.author}</span>
          </Typography>
          {props.location.pathname != '/profile'
            &&  <Typography className="pos" color="textSecondary" style={{ float: "right", marginBottom: 0, paddingBottom: 0, fontStyle: 'italic' }}>
                  Liked by {props.data.rating}
                </Typography>
          }
          { props.location.pathname != '/profile' && <div><br/><br/></div>}
          { props.location.pathname != '/profile' && (props.loggedIn
          &&  (props.user.liked_quotes.filter((element) => element.quote_id == (props.data._id && props.data._id.$oid || props.data.quote_id)).length
                && <Button size="small" disabled style={{padding: 0, minHeight: 0, minWidth: 0, float: "right", paddingBottom: 5}}><ThumbUp /></Button>
                || <Button size="small" onClick={like} style={{padding: 0, minHeight: 0, minWidth: 0, float: "right", paddingBottom: 5}}><ThumbUp color="primary"/></Button>)
          ||  <CardActions style={{padding: "0 0 5 0", minHeight: 0, minWidth: 0, float: "right"}}>
                <Tooltip id="tooltip-icon" title="You are not logged in.">
                  <div>
                    <Button size="small" disabled style={{padding: 0, minHeight: 0, minWidth: 0}}><ThumbUp /></Button>
                  </div>
                </Tooltip>
              </CardActions>)
          }
          {props.location.pathname != '/profile' && props.data.author == (props.user && props.user.username)
            &&  <Button size="small" onClick={deleteQuote} style={{padding: 0, minHeight: 0, minWidth: 0}}><Delete /></Button>}
          {props.location.pathname != '/profile' && props.data.author == (props.user && props.user.username)
            &&  <EditQuote data={props.data}/>}
        </CardContent> 
      </Card>
    </div>
  )
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyCard))