import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import MyCard from './Card.js'
import CircularProgress from '@material-ui/core/CircularProgress'
import { withRouter } from 'react-router'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
    marginTop: 50,
  },
  gridList: {
    width: '100%',
    height: 'auto'
  },
  subheader: {
    width: '100%',
  },
});

const CardList = (props) => {
  const { classes, cards } = props

  return (
    <div className={classes.root}>
      {!cards.length
        &&  <CircularProgress color='primary'/> }
      <GridList cellHeight={'auto'} className={classes.gridList} cols={props.location.pathname == '/profile' && 1 || 2}>
        {cards.length
          &&  cards.map(item => (
                  <GridListTile key={item.id} cols={1}>
                    <MyCard data={item} />
                  </GridListTile>
                )
            )
        }
      </GridList>
    </div>
  )
}

export default withRouter(withStyles(styles)(CardList))
