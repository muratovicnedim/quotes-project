import React, { Component } from 'react'
import { 
  Route,
  withRouter,
  Switch
} from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { fetchQuotes } from '../actions/index'
import CardList from './CardList'
import Login from './Login'
import Logout from './Logout'
import Registration from './Registration'
import Profile from './Profile'
import PrivateRoute from './PrivateRoute'
import AddQuote from './AddQuote'
import SortCriteria from './SortCriteria'

const mapStateToProps = state => {
  return { 
    user: state.user, 
    articles: state.articles, 
    quotes: state.quotes 
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchQuotes: bindActionCreators(fetchQuotes, dispatch)
  }
}

class Content extends Component { 
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.fetchQuotes().then(data => {
      // Do something if needed
    })
  }

  render() {
    return (
      <div>
        <Switch>
          <Route exact path='/' render={(props) =>
            <div>
              {this.props.user && <AddQuote />}
              <SortCriteria />
              <CardList cards={this.props.quotes} />
            </div>} 
          />
          <Route path='/login' component={Login} />
          <Route path='/register' component={Registration} />
          <Route path='/logout' component={Logout} />
          <PrivateRoute path='/profile' component={Profile} />
        </Switch>
      </div>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Content))
