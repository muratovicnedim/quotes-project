import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Edit from '@material-ui/icons/Edit'
import { editQuote } from '../actions/index'
import { connect } from 'react-redux'

const mapDispatchToProps = dispatch => {
  return {
    editQuote: (quote) => dispatch(editQuote(quote))
  }
}

class EditQuote extends React.Component {
 
  state = {
    open: false,
    title: this.props.data.title,
    text: this.props.data.text
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleTitleChange = (e) => {
    this.setState({
      title: e.target.value
    })
  }
  
  handleTextChange = (e) => {
    this.setState({
      text: e.target.value
    })
  }

  editQuote = () => {
    const quote = {
      id: this.props.data._id,
      title: this.state.title,
      text: this.state.text
    }
    this.props.editQuote(quote).then(data => {
      this.setState({ open: false });
    })
  }

  render() {
    return (
      <div style={{display : 'inline-block'}}>
        <Button size="small" onClick={this.handleClickOpen} style={{padding: 0, minHeight: 0, minWidth: 0}}><Edit /></Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Edit quote</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Below you can enter new title and text for your quote!
            </DialogContentText>
            <TextField
              value={this.state.title}
              onChange={this.handleTitleChange}
              margin="dense"
              id="title"
              label="Title"
              fullWidth
            />
            <TextField
              value={this.state.text}
              onChange={this.handleTextChange}
              margin="dense"
              id="description"
              label="Description"
              fullWidth
              multiline
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.editQuote} color="primary">
              Submit changes
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(EditQuote)