import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import SimpleMenu from './Menu'

const styles = {
  root: {
    flexGrow: 1,
  },
};

const mapStateToProps = (state) => {
  return { 
    loggedIn: state.user && true || false 
  }
}

const Header = (props) => {
  const { classes } = props
  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Toolbar style={{justifyContent: 'space-between'}}>
          <Typography variant="title" color="inherit">QUOTES</Typography>
          <SimpleMenu />
        </Toolbar>
      </AppBar>
    </div>
  )
}

export default connect(mapStateToProps, null)(withStyles(styles)(Header))
