import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { removeUser } from '../actions/index'

const mapDispatchToProps = dispatch => {
    return {
        removeUser: () => dispatch(removeUser())
    }
}

const Logout = (props) => {
    props.removeUser()
    return <Redirect to='/' />
}

export default connect(null, mapDispatchToProps)(Logout)