import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom'
import MenuIcon from '@material-ui/icons/Menu'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return { 
      loggedIn: state.user && true || false 
    }
  }

class SimpleMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;

    return (
      <div>
        <Button
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
            <MenuIcon />
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
        <Link to='/' style={{ textDecoration: 'none'}}>
            <MenuItem onClick={this.handleClose}>Home</MenuItem>
        </Link>
        {this.props.loggedIn
            &&  <Link to='/logout' style={{ textDecoration: 'none'}}>
                    <MenuItem onClick={this.handleClose}>Logout</MenuItem>
                </Link>
            ||  <Link to='/login' style={{ textDecoration: 'none'}}>
                    <MenuItem onClick={this.handleClose}>Login</MenuItem>
                </Link>
        }
        {!this.props.loggedIn 
            && <Link to='/register' style={{ textDecoration: 'none'}}>
                    <MenuItem onClick={this.handleClose}>Registration</MenuItem>
                </Link>}
        {this.props.loggedIn && 
            <Link to='/profile' style={{ textDecoration: 'none'}}>
            <MenuItem onClick={this.handleClose}>Profile</MenuItem>
            </Link>}
        </Menu>
      </div>
    );
  }
}

export default connect(mapStateToProps, null)(SimpleMenu);
