import React from 'react'
import { connect } from 'react-redux'
import CardList from './CardList'
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    marginTop: '2%'
  },
});

const mapStateToProps = state => {
  return {
    user: state.user,
    likedQuotes: state.user.liked_quotes
  }
}

const Profile = (props) => {
  const { classes } = props;
  return (
    <div>
    <Paper className={classes.root} elevation={1}>
        <Typography variant="headline" component="h3">
          Hi <span style={{color: 'blue', fontWeight: '430'}}>{props.user.username}</span>
        </Typography>
        <br/>
        <Typography component="p">
          This is your profile page where you can find quotes that you like.
        </Typography>
        <br/>
        <Typography variant="caption">
          <em>In future, we plan to add more features to your profile page.</em>
        </Typography>
      </Paper>
      <CardList cards={props.likedQuotes} />
    </div>
  )
}

export default connect(mapStateToProps, null)(withStyles(styles)(Profile))