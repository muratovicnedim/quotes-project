import React, { Component } from "react"
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import Button from "@material-ui/core/Button"
import TextField from "@material-ui/core/TextField"
import { registerUser } from '../actions/index'

const mapDispatchToProps = dispatch => {
  return {
    registerUser: (username, password) => dispatch(registerUser(username, password))
  }
}

const mapStateToProps = state => {
  return { 
    redirect: state.user && true 
  }
}

class Registration extends Component {
  constructor(props) {
    super(props)

    this.state = {
      username: '',
      password: ''
    }

    this.handleUsernameChange = this.handleUsernameChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
  }

  handleUsernameChange = (e) => {
    this.setState({
      ...this.state,
      username: e.target.value
    })
  }

  handlePasswordChange = (e) => {
    this.setState({
      ...this.state,
      password: e.target.value
    })
  }

  register = () => {
    this.props.registerUser(this.state.username, this.state.password).then(data => {
      // Do something if needed
    })
  }

  render() {
    if (this.props.redirect)
      return <Redirect to='/' />
    
    return (
      <div style={{ textAlign: 'center'}}>
        <TextField
          id="with-placeholder"
          label="username"
          margin="normal"
          value={this.state.username}
          onChange={this.handleUsernameChange}
        />
        <br />
        <TextField
          id="password-input"
          label="password"
          type="password"
          autoComplete="current-password"
          margin="normal"
          value={this.state.password}
          onChange={this.handlePasswordChange}
        />
        <br />
        <Button variant="raised" onClick={this.register}>Register</Button>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Registration)
