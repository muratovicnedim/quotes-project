import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { connect } from 'react-redux'
import { sortQuotes } from '../actions/index'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

const mapStateToProps = state => {
  return {
    quotes: state.quotes
  }
}

const mapDispatchToProps = dispatch => {
  return {
    sortQuotes: sortCriteria => dispatch(sortQuotes(sortCriteria))
  }
}

class SimpleSelect extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      criteria: ''
    }
  }

  handleChange = event => {
    this.props.sortQuotes(event.target.value)
    
    this.setState({ [event.target.name]: event.target.value });
  }

  render() {
    const { classes } = this.props;

    return (
      <form className={classes.root} autoComplete="off">
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="criteria-helper" style={{color: 'crimson', fontSize: 13}}>SORT BY</InputLabel>
          <Select
            value={this.state.criteria}
            onChange={this.handleChange}
            input={<Input name="criteria" id="criteria-helper" />}
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value="New">New</MenuItem>
            <MenuItem value="Best">Best</MenuItem>
          </Select>
          <FormHelperText><em>Select sort criteria</em></FormHelperText>
        </FormControl>
      </form>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SimpleSelect))
