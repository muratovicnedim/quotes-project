import { 
    REMEMBER_USER, 
    FETCH_QUOTES,
    REMOVE_USER,
    ADD_QUOTE,
    DELETE_QUOTE,
    SAVE_LIKED_QUOTE,
    UPDATE_QUOTE_LIKES,
    EDIT_QUOTE
} from '../constants/action-types'

const initialState = {
    user: null,
    quotes: [],
    lastAddedQuote: null
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case REMEMBER_USER:
            return { 
                ...state, 
                user: action.payload 
            }
        case REMOVE_USER:
            return { 
                ...state, 
                user: action.payload 
            }
        case ADD_QUOTE:
            return { 
                ...state, 
                quotes: [...state.quotes, action.payload],
                lastAddedQuote: action.payload
            }
        case DELETE_QUOTE:
                const indexOfElementToBeDeleted = state.quotes.findIndex(element => element._id == action.payload)
                if (indexOfElementToBeDeleted > -1)
                    return {
                        ...state,
                        quotes: [...state.quotes.filter((_, index) => index != indexOfElementToBeDeleted)]
                    }
        case EDIT_QUOTE:
            const indexOfElementToBeUpdated = state.quotes.findIndex(element => element._id == action.payload.quoteId)
            if (indexOfElementToBeUpdated > -1)
                    return {
                        ...state,
                        quotes: state.quotes.map((quote, i) => i == indexOfElementToBeUpdated ? {...quote, title: action.payload.title, text: action.payload.text} : quote)  
                    }
        case SAVE_LIKED_QUOTE:
            return { 
                ...state, 
                user: {
                    ...state.user, 
                    liked_quotes: [...state.user.liked_quotes, action.payload] 
                }
            }
        case UPDATE_QUOTE_LIKES:
            const index = state.quotes.findIndex(element => element._id == action.payload._id)
            return { 
                ...state, 
                quotes: [...state.quotes.slice(0, index), action.payload, ...state.quotes.slice(index + 1)]
            }
        case FETCH_QUOTES:
            return { 
                ...state, 
                quotes: action.payload 
            }
        case "SORT_QUOTES":
            const sortedQuotes = [...state.quotes]
            if (action.payload == 'New')
                sortedQuotes.sort((a, b) => new Date(b.date) - new Date(a.date))
            else if (action.payload == 'Best')
                sortedQuotes.sort((a, b) => b.rating - a.rating)
            
            return {
                ...state,
                quotes: sortedQuotes
            }
        default:
            return state
    }
}

export default rootReducer