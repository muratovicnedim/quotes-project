const path = require("path")

const DIST_DIR = path.resolve(__dirname, "dist")
const SRC_DIR = path.resolve(__dirname, "src")

const config = {
  entry: SRC_DIR + "/index.js",
  output: {
    path: DIST_DIR + "/",
    filename: "bundle.js",
    publicPath: "/",
    sourceMapFilename: 'bundle.map'
  },
  devtool: '#source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        include: SRC_DIR,
        exclude: /(node_modules)/,
        use: {
          loader: "babel-loader",
          query: {
            presets: ["react", "env", "stage-0"]
          }
        }
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
    ]
  }
}

module.exports = config