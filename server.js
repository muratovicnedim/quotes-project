const express = require('express');
const cors = require('cors')
const axios = require('axios')
require('dotenv').config()
const MongoClient = require('mongodb').MongoClient;
const { ObjectId } = require('mongodb')
// Connection URL
const url = 'mongodb://nmuratovic1:Seithae6poF0@ds261570.mlab.com:61570/quotes-db-etf';
// Database Name
const dbName = 'quotes-db-etf';

var mLab = require('mongolab-data-api')(process.env.API_KEY);

const app = express();
const port = process.env.PORT || 5000;

app.use(cors())

app.get('/api/login', (req, res) => {
    axios.get(`https://api.mlab.com/api/1/databases/quotes-db-etf/collections/users?q={"username": "${req.query.username}", "password": "${req.query.password}"}&fo=true&apiKey=${process.env.API_KEY}`)
        .then(response => {
            res.send(response.data)
        })
        .catch(err => {
            console.log(err)
        })
});

app.post('/api/register', (req, res) => {
    const user = {
        username: req.query.username,
        password: req.query.password,
        liked_quotes: new Array()
    }
    axios.post(`https://api.mlab.com/api/1/databases/quotes-db-etf/collections/users?apiKey=${process.env.API_KEY}`,
        user
    )
    .then(response => {
        res.send(response.data)
    })
    .catch(err => {
        console.log(err)
    })
})

app.post('/api/addquote', (req, res) => {
    console.log(req.query)
    axios.post(`https://api.mlab.com/api/1/databases/quotes-db-etf/collections/quotes?apiKey=${process.env.API_KEY}`,
        req.query
    ).then(response => {
        res.send(response.data)
    }).catch(err => {
        console.log(err)
    })
})

app.post('/api/deletequote', (req, res) => {
    console.log(req.query.quoteId)
    console.log(JSON.parse(req.query.quoteId).$oid)
    console.log('object id ' + ObjectId(JSON.parse(req.query.quoteId).$oid))
    console.log('hahahahaha')
    MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName)
        const quotes = db.collection('quotes')

        quotes.deleteOne({"_id": ObjectId(JSON.parse(req.query.quoteId).$oid)}, function(data, error) {
            if(error)
                console.log(error)
            
            console.log(data)

            res.send({
                message: 'Successfuly deleted'
            })
            client.close()
        })
    })
})

app.post('/api/editquote', (req, res) => {
    MongoClient.connect(url, function(err, client) {
        const db = client.db(dbName)
        const quotes = db.collection('quotes')

        quotes.updateOne(
            {"_id": ObjectId(JSON.parse(req.query.quoteId).$oid)},
            {$set: {"title": req.query.title, "text": req.query.text}},
            function(data, error) {
                if(error)
                    console.log(error)
                
                res.send({
                    message: 'Quote successfuly updated!'
                })
                client.close()
            }
        )
    })
})

app.post('/api/savequote', (req, res) => {
    const quote = {
        quote_id: JSON.parse(req.query._id).$oid,
        title: req.query.title,
        text: req.query.text,
        author: req.query.author,
        date: req.query.date,
        rating: parseInt(req.query.rating, 10) + 1
    }
    const userId = JSON.parse(req.query.userId).$oid

    // Use connect method to connect to the server
    MongoClient.connect(url, function(err, client) {
    console.log("Connected successfully to server");

    const db = client.db(dbName);
    const collection = db.collection('users')
    collection.update({_id: ObjectId(userId)}, {$push: {liked_quotes: quote}}, (error, result) => {
        const coll = db.collection('quotes')
        coll.update({_id: ObjectId(quote.quote_id)}, {$set: {rating: quote.rating}}, (err, response) => {
            res.send(quote)
            client.close();
        })
    })
    });
})

app.get('/api/quotes', (req, res) => {
    const options = {
        database: "quotes-db-etf",
        collectionName: "quotes"
    }

    mLab.listDocuments(options, (err, data) => {
        if(err)
            console.log(err)

        res.send(data)
    })
})

app.listen(port, () => console.log(`Listening on port ${port}`));